﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static Model.mHub;

namespace Model
{
    public class mChatHub
    {
        public class OnConnected
        {
            public int ProductId { get; set; }
            public string ClientId { get; set; }
            public string ClientName { get; set; }
            public string ConnectionId { get; set; }
            public string Ip { get; set; }
            public string Generic { get; set; }
        }
        public class GetUserRoomAndConnections
        {
            public int RoomId { get; set; }
            public List<string> Connrctions { get; set; }
        }
        public class SendMessage
        {
            public int RoomId { get; set; }
            public int HubUserId { get; set; }
            public string Content { get; set; }
            public string AttachmentsString { get; set; }
            public string hello { get; set; }
            public List<Attachments> Attachments { get; set; }
        }


    }
}
