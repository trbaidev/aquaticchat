﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class mGeneric
    {
        public class mApiResponse
        {
            public int ResponseCode { get; set; }
            public string ResponseMsg { get; set; }
            public dynamic ResponseData { get; set; }
        }
        public class mProdoductBasicReq
        {
            public int ProductId { get; set; }
        }
        public class mDropDown
        {
            public string Id { get; set; }
            public string Value { get; set; }
            public dynamic Generic { get; set; }
        }
        public class mPagingBasicReq
        {
            public int PageSize { get; set; }
            public int PageNo { get; set; }
            public string OrderByKey { get; set; }
            public int OrderByVal { get; set; }
        }
        public class mPagingBasicResp
        {
            public int TotalRecords { get; set; }
            public int CurrentCount { get; set; }
        }
    }
}
