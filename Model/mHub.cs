﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mGeneric;

namespace Model
{
    public class mHub
    {
        public class CreateRoomReq
        {
            public string RoomName { get; set; }
            public int RoomType { get; set; }
            public int HubUserId { get; set; }
            public List<CreateRoomReqUsers> Users { get; set; }
            public int ProductId { get; set; }
        }
        public class DeleteRoomsReq
        {
            public int HubUserId { get; set; }
            public List<int> RoomIds { get; set; }
        }
        public class CreateRoomReqUsers
        {
            public string ClientId { get; set; }
            public string ClientName { get; set; }
            public string Generic { get; set; }
        }
        public class SendMsgViaAPIReq
        {
            public string Content { get; set; }
            public string RoomName { get; set; }
            public SendMsgViaAPIReqUserData MessageFrom { get; set; }
            public List<SendMsgViaAPIReqUserData> MessageTo { get; set; }
            public int ProductId { get; set; }
        }
        public class SendMsgViaAPIReqUserData
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string AllowedOnCampaignsId { get; set; }
            public string Generic { get; set; }
        }
        //public class Message
        //{
        //    public string Content { get; set; }
        //    public int HubUserFromId { get; set; }
        //    public string MessageFromName { get; set; }
        //    public int RoomId { get; set; }
        //    public string Attachment { get; set; }
        //    public string RoomName { get; set; }
        //    public List<mDropDown> CampaignsList { get; set; }
        //}
        public class GetUserRoomsByUsersIdReq
        {
            public string RoomName { get; set; }
            public int HubUserId { get; set; }
            public int ProductId { get; set; }
            public bool? IsFavorite { get; set; }
            public int CampaignId { get; set; }
            public int PageNo { get; set; }
            public int Take { get; set; }
        }
        public class GetUserRoomsByUsersIdResp
        {
            public int Count { get; set; }
            public List<GetUserRoomsByUsersIdDbResp> Data { get; set; }
        }
        public class GetUserRoomsByUsersIdDbResp
        {
            public bool Is_Favourite { get; set; }
            public MessageOldChats LastMessage { get; set; }
            public List<ChatInfo> ChatInfo { get; set; }
            public int RoomId { get; set; }
            public string RoomName { get; set; }
            public int CampaignId { get; set; }
            public int? StatusId { get; set; }
            public int HubUserId { get; set; }
            public string Color { get; set; }
            public string CampaignName { get; set; }
        }
        public class ChatInfo
        {
            public string Generic { get; set; }
            public string Name { get; set; }
        }
        public class MessageOldChats
        {
            public string Content { get; set; }
            public DateTime? Date { get; set; }
            public string Attachment { get; set; }
            public string ClientName { get; set; }
            public string Generic { get; set; }
        }

        public class GetChatDetailsByRoomIdReq
        {
            public int RoomId { get; set; }
            public int HubUserId { get; set; }
            public int ProductId { get; set; }
            public string AllowedOnCampaignsId { get; set; }
            public int Take { get; set; }
            public int Skip { get; set; }
        }
        public class MessageChat
        {
            public int RoomId { get; set; }
            public string Content { get; set; }
            public DateTime? Date { get; set; }
            public string Name { get; set; }
            public List<Attachments> Attachements { get; set; }
            public int HubUserId { get; set; }
            public string RoomName { get; set; }
            public bool Is_Favourite { get; set; }
            public List<mDropDown> CampaignsList { get; set; }
        }
        public class Attachments
        {
            public string Name { get; set; }
            public string Path { get; set; }
        }
        public class DownloadFileReq
        {
            public string Path { get; set; }
        }
        public class MakeRoomFavoriteReq
        {
            public int RoomId { get; set; }
            public bool IsFavorite { get; set; }
            public int HubUserId { get; set; }
        }
        public class MakeRoomFavoriteBulkReq
        {
            public List<int> RoomIds { get; set; }
            public bool IsFavorite { get; set; }
            public int HubUserId { get; set; }
        }
        public class SendGreetingMessageReq
        {
            public int HubUserId { get; set; }
            public int RoomId { get; set; }
            public int ProductId { get; set; }
            public string AllowedOnCampaignsId { get; set; }
        }
        public class UpdateCampaignIdInRoomReq
        {
            public int RoomId { get; set; }
            public int HubUserId { get; set; }
            public int CampaignId { get; set; }
        }
        public class GetCampaignsDdlReq
        {
            public string AllowedOnCampaignsId { get; set; }
            public int ProductId { get; set; }
        }
        public class UpdateConversationStatusInRoom
        {
            public int RoomId { get; set; }
            public int StatusId { get; set; }
        }
    }
}
