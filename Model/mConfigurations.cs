﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mGeneric;

namespace Model
{
    public class mConfigurations
    {
        public class mGetConfigurationStatusReq : mProdoductBasicReq
        {
        }
        public class mEnableConversationsReq : mProdoductBasicReq
        {
            public int IsActive { get; set; }
        }
        public class mGetAllCampaignsReq : mProdoductBasicReq
        {

        }
        public class mGetAllCampaignsResp
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public bool IsEnabled { get; set; }
            public string Discription { get; set; }
            public int IsActive { get; set; }
            public int Id { get; set; }
        }
        public class mEnableCampaignsReq : mProdoductBasicReq
        {
            public int CampaignId { get; set; }
            public bool IsEnabled { get; set; }
        }
        public class mAddUpdateCampaignSettingsReq : mProdoductBasicReq
        {
            public int CampaignId { get; set; }
            public string NameInternal { get; set; }
            public string NamePublic { get; set; }
            public string Color { get; set; }
            public string ShortDescription { get; set; }
            public List<string> Channels { get; set; }
            public bool IsPublic { get; set; }
            public bool IsGroupChat { get; set; }
            public bool IsDeplayedInAdminInbox { get; set; }
            public List<mConversationPermissions> Permissions { get; set; }
        }
        
        public class mConversationPermissions
        {
            public string Name { get; set; }
            public List<string> Values { get; set; }
        }
        public class GetCampaignSettingsByCampaignIdReq : mProdoductBasicReq
        {
            public int CampaignId { get; set; }
        }
        public class GetCampaignSettingsResp
        {
            public string Color { get; set; }
            public bool IsDeplayedInAdminInbox { get; set; }
            public bool IsGroupChat { get; set; }
            public bool IsPublic { get; set; }
            public string NameInternal { get; set; }
            public string NamePublic { get; set; }
            public string ShortDescription { get; set; }
            public List<string> Channels { get; set; }
            public List<mConversationPermissions> Permissions { get; set; }
        }
        public class mAddUpdateProductNotificationSettingsReq : mProdoductBasicReq
        {
            public int HubUserId { get; set; }
            public List<mCampaignNotificationsSettings> ProductNotificationSettings { get; set; }
            public List<mCampaignNotificationsSettings> UserNotificationSettings { get; set; }

        }
        public class mCampaignNotificationsSettings
        {
            public int CampaignId { get; set; }
            public bool IsGlobal { get; set; }
        }
        public class GetAllCampaignsGridReq
        {
            public int ProductId { get; set; }
            public string CampaignName { get; set; }
            public mPagingBasicReq Pagination { get; set; }

        }
        public class GetAllCampaignsGridDbResp
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public bool IsDefault { get; set; }
            public string ConversationSize { get; set; }
            public List<string> Eligibility { get; set; }
        }
        public class GetAllCampaignsGridResp
        {
            public List<GetAllCampaignsGridDbResp> Data { get; set; }
            public mPagingBasicResp Pagination { get; set; }
        }
        public class GetConversationStatusesReq: mProdoductBasicReq
        {
            public string StatusName { get; set; }
        }
        public class GetConversationStatusesResp
        {
            public string NameInternal { get; set; }
            public string Action { get; set; }
            public bool IsDefault { get; set; }
            public int Id { get; set; }
        }

        public class GetCampaignsddlWithCountReq
        {
            public string AllowedOnCampaignsId { get; set; }
            public int ProductId { get; set; }
            public int HubUserId { get; set; }
        }
    }
}
