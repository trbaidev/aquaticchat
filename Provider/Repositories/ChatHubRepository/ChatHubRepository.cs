﻿using DB.SQL.Context;
using DB.SQL.Context.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using static Model.mChatHub;
using static Model.mHub;

namespace Provider.Repositories.ChatHubRepository
{
    public class ChatHubRepository : IChatHubRepository
    {
        private ACDbContext DBContext;

        public ChatHubRepository(ACDbContext _dbContext)
        {
            DBContext = _dbContext;
        }

        public int GetHubUserId(OnConnected OC)
        {
            try
            {


                Users_TB users_TB = DBContext.Users_TB.Where(x => x.ClientId == OC.ClientId && x.ProductId == OC.ProductId).FirstOrDefault();
                if (users_TB != null)
                {
                    users_TB.ClientName = OC.ClientName;
                    users_TB.Generic = string.IsNullOrEmpty(OC.Generic) ? users_TB.Generic : OC.Generic;
                    users_TB.UpdatedOn = DateTime.Now;
                    DBContext.SaveChanges();
                }
                else
                {
                    users_TB = new Users_TB
                    {
                        ClientId = OC.ClientId,
                        ClientName = OC.ClientName,
                        CreatedOn = DateTime.Now,
                        IsActive = 1,
                        ProductId = OC.ProductId,
                        Generic = OC.Generic
                    };
                    DBContext.Users_TB.Add(users_TB);
                    DBContext.SaveChanges();
                }
                if (!string.IsNullOrEmpty(OC.ConnectionId))
                {
                    UserConnection_TB userConnection_TB = new UserConnection_TB
                    {
                        CreatedOn = DateTime.Now,
                        Ip = OC.Ip,
                        UserId = users_TB.Id,
                        ConnectionId = OC.ConnectionId,
                        IsActive = 1
                    };
                    DBContext.UserConnection_TB.Add(userConnection_TB);
                    DBContext.SaveChanges();
                }


                return users_TB.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public void RemoveUserConnection(string ConnectionId)
        {
            IQueryable<UserConnection_TB> UT = DBContext.UserConnection_TB.Where(x => x.ConnectionId == ConnectionId);
            foreach (UserConnection_TB item in UT)
            {
                item.IsActive = 0;
            }
            DBContext.SaveChanges();
        }

        public Tuple<List<string>, MessageChat> SaveMessageAndGetConnectionIds(SendMessage Req)
        {
            List<string> ConnectionIds = ConnectionIds = DBContext.UserRoom_TB.Where(x => x.IsActive == 1 && x.RoomId == Req.RoomId)
                .Select(x => x.Users_TB.UserConnection_TBs.Where(y => y.IsActive == 1)
                .Select(y => y.ConnectionId).ToList())
                .ToList().SelectMany(x => x).ToList();


            DateTime TempDate = DateTime.Now;
            Messages_TB messages_TB = new Messages_TB
            {
                Content = Req.Content,
                CreatedBy = Req.HubUserId,
                CreatedOn = TempDate,
                IsActive = 1,
                RoomId = Req.RoomId,
                UserId = Req.HubUserId
            };
            if (Req.Attachments != null)
            {
                messages_TB.MessageAttachments_TB = Req.Attachments.Select(x => new MessageAttachments_TB
                {
                    CreatedOn = TempDate,
                    CreatedBy = Req.HubUserId,
                    IsActive = 1,
                    Name = x.Name,
                    Path = x.Path
                }).ToList();
            }
            DBContext.Messages_TB.Add(messages_TB);
            DBContext.SaveChanges();
            return new Tuple<List<string>, MessageChat>(ConnectionIds, new MessageChat
            {
                RoomId = Req.RoomId,
                Attachements = Req.Attachments == null ? new List<Attachments>() : Req.Attachments,
                Content = Req.Content,
                Date = TempDate,
                HubUserId = Req.HubUserId,
                Name = DBContext.Users_TB.Where(x => x.Id == Req.HubUserId).Select(x => x.ClientName).FirstOrDefault()
            });
        }
        

    }
    


}

