﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mChatHub;
using static Model.mHub;

namespace Provider.Repositories.ChatHubRepository
{
    public interface IChatHubRepository
    {
        int GetHubUserId(OnConnected OC);
        void RemoveUserConnection(string ConnectionId);
        Tuple<List<string>, MessageChat> SaveMessageAndGetConnectionIds(SendMessage Req);
    }
}
