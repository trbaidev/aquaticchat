﻿using DB.SQL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider.Repositories.GenericRepository
{
    public class GenericRepository : IGenericRepository
    {
        private ACDbContext DBContext;

        public GenericRepository(ACDbContext _dbContext)
        {
            DBContext = _dbContext;
        }
    }
}
