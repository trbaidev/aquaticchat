﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mGeneric;
using static Model.mHub;

namespace Provider.Repositories.HubRepository
{
    public interface IHubRepository
    {
        int CreateRoom(CreateRoomReq Req);
        Tuple<List<string>, MessageChat> SendMsgViaAPI(SendMsgViaAPIReq Req);
        mApiResponse GetUserRoomsByUsersId(GetUserRoomsByUsersIdReq Req);
        mApiResponse GetChatDetailsByRoomId(GetChatDetailsByRoomIdReq Req);
        mApiResponse DeleteRooms(DeleteRoomsReq Req);
        mApiResponse MakeRoomFavorite(MakeRoomFavoriteReq Req);
        mApiResponse MakeRoomFavoriteBulk(MakeRoomFavoriteBulkReq req);
        Tuple<List<string>, MessageChat> SendGreetingMessage(SendGreetingMessageReq req);
        Tuple<List<string>, MessageChat> UpdateCampaignIdInRoom(UpdateCampaignIdInRoomReq req);
        List<mDropDown> GetCampaignsDdl(GetCampaignsDdlReq req);
        mApiResponse UpdateConversationStatusInRoom(UpdateConversationStatusInRoom req);
    }
}
