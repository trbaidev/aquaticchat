﻿using DB.SQL.Context;
using DB.SQL.Context.Tables;
using Provider.Repositories.ChatHubRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mChatHub;
using static Model.mGeneric;
using static Model.mHub;

namespace Provider.Repositories.HubRepository
{
    public class HubRepository: IHubRepository
    {
        private ACDbContext DBContext;
        private IChatHubRepository ChatRepo;

        public HubRepository(ACDbContext _dbContext, IChatHubRepository _ChatRepo)
        {
            DBContext = _dbContext;
            ChatRepo = _ChatRepo;
        }

        public int CreateRoom(CreateRoomReq Req)
        {
            List<Users_TB> users_TBs = DBContext.Users_TB
                .Where(x => Req.Users.Select(y => y.ClientId).ToList().Contains(x.ClientId) && x.IsActive == 1)
                .ToList();
            List<Users_TB> NewUsers = new List<Users_TB>();
            foreach (CreateRoomReqUsers item in Req.Users)
            {
                Users_TB UT = users_TBs.Where(x => x.ClientId == item.ClientId).FirstOrDefault();
                if (UT == null)
                {
                    NewUsers.Add(new Users_TB
                    {
                        ClientId = item.ClientId,
                        ClientName = item.ClientName,
                        CreatedOn = DateTime.Now,
                        IsActive = 1,
                        ProductId = Req.ProductId,
                        Generic = item.Generic
                    });
                }
                else
                {
                    UT.ClientName = item.ClientName;
                    UT.Generic = item.Generic;
                    UT.UpdatedOn = DateTime.Now;
                }
            }
            if (NewUsers.Count > 0)
            {
                DBContext.Users_TB.AddRange(NewUsers);
                DBContext.SaveChanges();
            }
            var rooms_TBCheckQuery = DBContext.UserRoom_TB.Where(x => x.Rooms_TB.IsActive == 1 
            && x.Rooms_TB.Name == Req.RoomName && x.UserId == Req.HubUserId);
            foreach (Users_TB item in users_TBs)
            {
                rooms_TBCheckQuery = rooms_TBCheckQuery.Where(x => x.UserId == item.Id);
            }
            foreach (Users_TB item in NewUsers)
            {
                rooms_TBCheckQuery = rooms_TBCheckQuery.Where(x => x.UserId == item.Id);
            }
            //if (Req.RoomType == 1)
            //{
            //    string[] RoomNameSplit = Req.RoomName.Split("||");
            //    if (RoomNameSplit.Length < 1)
            //    {
            //        return 0;
            //    }
            //    else
            //    {
            //        rooms_TBCheckQuery = rooms_TBCheckQuery.Where(x => x.Name == Req.RoomName || x.Name == RoomNameSplit[1] + "||" + RoomNameSplit[0]);
            //    }
            //}
            //else
            //{
            //    rooms_TBCheckQuery = rooms_TBCheckQuery.Where(x => x.Name == Req.RoomName);
            //}
            UserRoom_TB rooms_TBCheck = rooms_TBCheckQuery.FirstOrDefault();
            if (rooms_TBCheck != null)
            {
                return rooms_TBCheck.RoomId;
            }




            Rooms_TB rooms_TB = new Rooms_TB
            {
                ProductId = Req.ProductId,
                Name = Req.RoomName,
                Type = Req.RoomType,
                CreatedOn = DateTime.Now,
                CreatedBy = Req.HubUserId,
                IsActive = 1,
                StatusId = DBContext.ConversationStatus_TB.Where(x => x.ProductId == Req.ProductId && x.IsDefaultForNewMsgs).Select(x => x.Id).FirstOrDefault()
            };
            List<UserRoom_TB> userRoom_TBs = users_TBs.Select(x => new UserRoom_TB
            {
                CreatedOn = DateTime.Now,
                CreatedBy = Req.HubUserId,
                IsActive = 1,
                UserId = x.Id,
                DeleteDate = DateTime.Now
            }).ToList();

            userRoom_TBs.AddRange(NewUsers.Select(x => new UserRoom_TB
            {
                CreatedOn = DateTime.Now,
                CreatedBy = Req.HubUserId,
                IsActive = 1,
                UserId = x.Id,
                DeleteDate = DateTime.Now
            }).ToList());
            userRoom_TBs.Add(new UserRoom_TB
            {
                CreatedOn = DateTime.Now,
                CreatedBy = Req.HubUserId,
                IsActive = 1,
                UserId = Req.HubUserId,
                DeleteDate = DateTime.Now
            });
            rooms_TB.UserRoom_TBs = userRoom_TBs;
            DBContext.Rooms_TB.Add(rooms_TB);
            DBContext.SaveChanges();
            return rooms_TB.Id;


        }
        public mApiResponse DeleteRooms(DeleteRoomsReq Req)
        {
            IQueryable<UserRoom_TB> Query = DBContext.UserRoom_TB.Where(x => x.UserId == Req.HubUserId && Req.RoomIds.Contains(x.RoomId));
            foreach (UserRoom_TB item in Query)
            {
                item.DeleteDate = DateTime.Now;
            }
            DBContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Chats Deleted Successfully"
            };
        }
        public Tuple<List<string>, MessageChat> SendMsgViaAPI(SendMsgViaAPIReq Req)
        {
            OnConnected onConnected = new OnConnected
            {
                ClientId = Req.MessageFrom.Id,
                ClientName = Req.MessageFrom.Name,
                ProductId = Req.ProductId,
                Generic = Req.MessageFrom.Generic
            };
            int HubUserId = ChatRepo.GetHubUserId(onConnected);
            CreateRoomReq createRoomReq = new CreateRoomReq
            {
                HubUserId = HubUserId,
                ProductId = Req.ProductId,
                RoomName = Req.RoomName,
                RoomType = 1,
                Users = Req.MessageTo.Select(x => new CreateRoomReqUsers
                {
                    ClientId = x.Id,
                    ClientName = x.Name,
                    Generic = x.Generic
                }).ToList()
            };
            int CreateRoomResp = CreateRoom(createRoomReq);
            Messages_TB messages_TB = new Messages_TB
            {
                Content = Req.Content,
                CreatedBy = HubUserId,
                CreatedOn = DateTime.Now,
                IsActive = 1,
                RoomId = CreateRoomResp,
                UserId = HubUserId
            };
            DBContext.Messages_TB.Add(messages_TB);
            DBContext.SaveChanges();
            List<string> ClientIds = Req.MessageTo.Select(x => x.Id).ToList();
            ClientIds.Add(Req.MessageFrom.Id);
            List<string> UserConnections = DBContext.UserConnection_TB.Where(x => ClientIds.Contains(x.Users_TB.ClientId))
                .Select(x => x.ConnectionId).ToList();
            return new Tuple<List<string>, MessageChat>(UserConnections, new MessageChat
            {
                Content = Req.Content,
                HubUserId = HubUserId,
                Name = Req.MessageFrom.Name,
                RoomName = Req.RoomName,
                RoomId = CreateRoomResp,
                Attachements = new List<Attachments>(),
                Date = DateTime.Now,
                Is_Favourite = false
            });
        }


        public Tuple<List<string>, MessageChat> SendGreetingMessage(SendGreetingMessageReq req)
        {
            Users_TB users_TB = DBContext.Users_TB.Where(x => x.Id == req.HubUserId).FirstOrDefault();
            Rooms_TB rooms_TB = DBContext.Rooms_TB.Where(x => x.Id == req.RoomId).FirstOrDefault();
            Products_TB products_TB = DBContext.Products_TB.Where(x => x.Id == req.ProductId).FirstOrDefault();
            List<string> ConnectionIds = DBContext.UserConnection_TB.Where(x => x.UserId == req.HubUserId).Select(x => x.ConnectionId).ToList();
            string GreetingMessages = products_TB.GreetingMsg.Replace("#CustomerName#", users_TB.ClientName);
            if(!string.IsNullOrEmpty(products_TB.GreetingMsg))
            {
                Messages_TB messages_TB = new Messages_TB
                {
                    Content = GreetingMessages,
                    CreatedBy = 0,
                    CreatedOn = DateTime.Now,
                    IsActive = 1,
                    RoomId = req.RoomId
                };
                DBContext.Messages_TB.Add(messages_TB);
                DBContext.SaveChanges();
                List<mDropDown> CampaignsData = new List<mDropDown>();
                if(!string.IsNullOrEmpty(req.AllowedOnCampaignsId))
                {
                    CampaignsData = DBContext.Campaigns_TB
                        .Where(x => x.CampaignPermissions_TBs.Where(x => x.Value == req.AllowedOnCampaignsId.ToString()).Count() > 0 && x.IsActive == 1 && x.IsEnabled == true)
                        .Select(x => new mDropDown
                        {
                            Id = x.Id.ToString(),
                            Value = x.Name.ToString()
                        }).ToList();
                } else
                {
                    CampaignsData = DBContext.Campaigns_TB
                        .Where(x => x.IsActive == 1 && x.IsEnabled == true)
                        .Select(x => new mDropDown
                        {
                            Id = x.Id.ToString(),
                            Value = x.Name.ToString()
                        }).ToList();
                }
                return new Tuple<List<string>, MessageChat>(ConnectionIds, new MessageChat
                {
                    Content = GreetingMessages,
                    HubUserId = 0,
                    Name = products_TB.GreetingMsgFrom,
                    RoomName = rooms_TB.Name,
                    RoomId = req.RoomId,
                    CampaignsList = CampaignsData,
                    Attachements = new List<Attachments>(),
                    Date = DateTime.Now,
                    Is_Favourite = false
                });
            } else
            {
                return new Tuple<List<string>, MessageChat>(new List<string>(), new MessageChat { });
            }
            
        }
        public List<mDropDown> GetCampaignsDdl(GetCampaignsDdlReq req)
        {
            List<mDropDown> CampaignsData = new List<mDropDown>();
            if (!string.IsNullOrEmpty(req.AllowedOnCampaignsId))
            {
                CampaignsData = DBContext.Campaigns_TB
                    .Where(x => x.CampaignPermissions_TBs.Where(x => x.Value == req.AllowedOnCampaignsId.ToString()).Count() > 0 && x.IsActive == 1 && x.IsEnabled == true)
                    .Select(x => new mDropDown
                    {
                        Id = x.Id.ToString(),
                        Value = x.Name.ToString(),
                        Generic = x.CampaignSettings_TB.Color
                    }).ToList();
            }
            else
            {
                CampaignsData = DBContext.Campaigns_TB
                    .Where(x => x.IsActive == 1 && x.IsEnabled == true)
                    .Select(x => new mDropDown
                    {
                        Id = x.Id.ToString(),
                        Value = x.Name.ToString(),
                        Generic = x.CampaignSettings_TB.Color
                    }).ToList();
            }
            return CampaignsData;
        }
        public Tuple<List<string>, MessageChat> UpdateCampaignIdInRoom(UpdateCampaignIdInRoomReq req)
        {
            List<string> ConnectionIds = DBContext.UserConnection_TB.Where(x => x.UserId == req.HubUserId).Select(x => x.ConnectionId).ToList();
            Users_TB users_TB = DBContext.Users_TB.Where(x => x.Id == req.HubUserId).FirstOrDefault();
            Campaigns_TB campaigns_TB = DBContext.Campaigns_TB.Where(x => x.Id == req.CampaignId).FirstOrDefault();
            Rooms_TB rooms_TB = DBContext.Rooms_TB.Where(x => x.Id == req.RoomId).FirstOrDefault();
            rooms_TB.CampaignId = req.CampaignId;
            Messages_TB messages_TB = new Messages_TB
            {
                Content = "You Selected:" + campaigns_TB.Name,
                CreatedBy = req.HubUserId,
                CreatedOn = DateTime.Now,
                IsActive = 1,
                RoomId = req.RoomId,
                UserId = req.HubUserId
            };
            DBContext.Messages_TB.Add(messages_TB);
            DBContext.SaveChanges();
            return new Tuple<List<string>, MessageChat>(ConnectionIds, new MessageChat
            {
                Content = messages_TB.Content,
                HubUserId = req.HubUserId,
                Name = users_TB.ClientName,
                RoomName = rooms_TB.Name,
                RoomId = req.RoomId,
                Date = DateTime.Now
            });
        }

        public mApiResponse GetUserRoomsByUsersId(GetUserRoomsByUsersIdReq Req)
        {
          
            var query = (from t1 in DBContext.Users_TB
                         join t2 in DBContext.UserRoom_TB
                         on t1.Id equals t2.UserId
                         join t3 in DBContext.Rooms_TB
                         on t2.RoomId equals t3.Id 
                         where t1.IsActive == 1 && t2.IsActive == 1 && t3.IsActive == 1
                         && t3.Messages_TBs.Where(x => x.CreatedOn > t2.DeleteDate).Count() > 0
                         select new
                         {
                             ProductId = t1.ProductId,
                             RoomId = t3.Id,
                             RoomName = t3.Name,
                             CampaignId = t3.CampaignId == null ? 0 : t3.CampaignId.Value,
                             CampaignColor = t3.Campaigns_TB == null ? "" : t3.Campaigns_TB.CampaignSettings_TB.Color,
                             CampaignName = t3.Campaigns_TB == null ? "" : t3.Campaigns_TB.CampaignSettings_TB.NamePublic,
                             ChatInfo = t3.UserRoom_TBs.Where(x => x.UserId != Req.HubUserId)
                             .Select(x => new ChatInfo {
                                  Generic = x.Users_TB.Generic,
                                  Name = x.Users_TB.ClientName
                             }).ToList(),        
                             Is_Favourite = t2.Is_Favourite,
                             LastMessage = t3.Messages_TBs.OrderByDescending(x => x.CreatedOn)
                             .Select(x => new MessageOldChats
                             {
                                 Content = x.Content,
                                 Date = x.CreatedOn,
                                 Attachment = "",
                                 ClientName = x.Users_TB == null ? "AAV BOT": x.Users_TB.ClientName,
                                 Generic = x.Users_TB.Generic
                             }).FirstOrDefault(),
                             StatusId = t3.StatusId,
                             HubUserId = t2.UserId
                         }); 
            if(Req.ProductId != 0 && Req.HubUserId == 0)
            {
                query = (from t1 in DBContext.Rooms_TB
                         where t1.IsActive == 1 && t1.ProductId == Req.ProductId
                         select new
                         {
                             ProductId = t1.ProductId,
                             RoomId = t1.Id,
                             RoomName = t1.Name,
                             CampaignId = t1.CampaignId == null ? 0 : t1.CampaignId.Value,
                             CampaignColor = t1.Campaigns_TB == null ? "" : t1.Campaigns_TB.CampaignSettings_TB.Color,
                             CampaignName = t1.Campaigns_TB == null ? "" : t1.Campaigns_TB.CampaignSettings_TB.NamePublic,
                             ChatInfo = t1.UserRoom_TBs.Where(x => x.UserId != Req.HubUserId)
                             .Select(x => new ChatInfo
                             {
                                 Generic = x.Users_TB.Generic,
                                 Name = x.Users_TB.ClientName
                             }).ToList(),
                             Is_Favourite = false,
                             LastMessage = t1.Messages_TBs.OrderByDescending(x => x.CreatedOn)
                             .Select(x => new MessageOldChats
                             {
                                 Content = x.Content,
                                 Date = x.CreatedOn,
                                 Attachment = "",
                                 ClientName = x.Users_TB == null ? "AAV BOT" : x.Users_TB.ClientName,
                                 Generic = x.Users_TB.Generic
                             }).FirstOrDefault(),
                             StatusId = t1.StatusId,
                             HubUserId = 0
                         });
            }
            if(Req.HubUserId != 0)
            {
                query = query.Where(x => x.HubUserId == Req.HubUserId);
            }
            if(Req.IsFavorite != null)
            {
                query = query.Where(x => x.Is_Favourite == Req.IsFavorite);
            }
            if(!string.IsNullOrEmpty(Req.RoomName))
            {
                query = query.Where(x => x.RoomName.Contains(Req.RoomName));
            }
            if(Req.CampaignId != 0)
            {
                query = query.Where(x => x.CampaignId == Req.CampaignId);
            }
            int Count = query.Count();

            List<GetUserRoomsByUsersIdDbResp> Data = query
                .Select(x => new GetUserRoomsByUsersIdDbResp { 
                    CampaignId = x.CampaignId,
                    ChatInfo = x.ChatInfo,
                    HubUserId = x.HubUserId,
                    Is_Favourite = x.Is_Favourite,
                    LastMessage = x.LastMessage,
                    RoomId = x.RoomId,
                    RoomName = x.RoomName,
                    StatusId = x.StatusId,
                    Color = x.CampaignColor,
                    CampaignName = x.CampaignName
                })
                .OrderByDescending(x => x.LastMessage.Date).ThenBy(x => x.Is_Favourite).Skip((Req.PageNo * Req.Take)).Take(Req.Take).ToList();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = new GetUserRoomsByUsersIdResp
                {
                    Count = Count,
                    Data = Data.Distinct().ToList()
                }
            };

        }

        public mApiResponse GetChatDetailsByRoomId(GetChatDetailsByRoomIdReq Req)
        {
            if(Req.RoomId == 0)
            {
                return new mApiResponse
                {
                    ResponseCode = 1,
                    ResponseMsg = "Success",
                    ResponseData = new List<MessageChat>()
                };
            }
            Rooms_TB rooms_TB = DBContext.Rooms_TB.Where(x => x.Id == Req.RoomId).FirstOrDefault();
            List<mDropDown> CampaignDdl;
            
            Products_TB products_TB = DBContext.Products_TB.Where(x => x.Id == Req.ProductId).FirstOrDefault();
            DateTime DeletedDate = DBContext.UserRoom_TB.Where(x => x.RoomId == Req.RoomId && x.UserId == Req.HubUserId)
                .Select(x => x.DeleteDate.Value).FirstOrDefault();
            List<MessageChat> dbResp = DBContext.Messages_TB
                .Where(x => x.RoomId == Req.RoomId && x.IsActive == 1 && x.CreatedOn > DeletedDate)
                .OrderByDescending(x => x.CreatedOn).Skip(Req.Skip).Take(Req.Take)
                .Select(x => new MessageChat
                {
                   RoomId = x.RoomId,
                   Content = x.Content,
                   Date = x.CreatedOn,
                   Name = x.Users_TB == null ? products_TB.GreetingMsgFrom : x.Users_TB.ClientName,
                   HubUserId = x.Users_TB == null ? 0 : x.Users_TB.Id,
                   RoomName = x.Rooms_TB.Name,
                   Is_Favourite = x.Rooms_TB.UserRoom_TBs.Where(y => y.UserId == Req.HubUserId).Select(y => y.Is_Favourite).FirstOrDefault(),
                   Attachements = x.MessageAttachments_TB.Where(y => y.IsActive == 1).Select(y => new Attachments
                   {
                       Name = y.Name,
                       Path = y.Path
                   }).ToList()
                }).OrderBy(x => x.Date).ToList();
            if (rooms_TB.CampaignId == null && Req.HubUserId == rooms_TB.CreatedBy)
            {
                CampaignDdl = GetCampaignsDdl(new GetCampaignsDdlReq
                {
                    AllowedOnCampaignsId = Req.AllowedOnCampaignsId,
                    ProductId = Req.ProductId
                });
                dbResp.ForEach(x => x.CampaignsList = (x.HubUserId == 0 ? CampaignDdl : null));
            }
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = dbResp
            };
        }
        public mApiResponse MakeRoomFavorite(MakeRoomFavoriteReq Req)
        {
            UserRoom_TB rooms_TB = DBContext.UserRoom_TB.Where(x => x.RoomId == Req.RoomId && x.UserId == Req.HubUserId).FirstOrDefault();
            if(rooms_TB != null)
            {
                rooms_TB.Is_Favourite = Req.IsFavorite;
                DBContext.SaveChanges();
                return new mApiResponse
                {
                    ResponseCode = 1,
                    ResponseMsg = "Room Is Marked " + (Req.IsFavorite ? "Favorite" : "Un-Favorite") + "",
                    ResponseData = Req.RoomId
                };
            } else
            {
                return new mApiResponse
                {
                    ResponseCode = 0,
                    ResponseMsg = "Room Not Found"
                };
            }
            
        }
        public mApiResponse MakeRoomFavoriteBulk(MakeRoomFavoriteBulkReq req)
        {
            IQueryable<UserRoom_TB> rooms_TB = DBContext.UserRoom_TB.Where(x => req.RoomIds.Contains(x.RoomId)  && x.UserId == req.HubUserId);
            foreach (var item in rooms_TB)
            {
                item.Is_Favourite = req.IsFavorite;
            }
                
            DBContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Rooms Are Marked " + (req.IsFavorite ? "Favorite" : "Un-Favorite") + "",
                ResponseData = req.RoomIds
            };
        }
        public mApiResponse UpdateConversationStatusInRoom(UpdateConversationStatusInRoom req)
        {
            IQueryable<Rooms_TB> query = DBContext.Rooms_TB.Where(x => x.Id == req.RoomId);
            foreach (var item in query)
            {
                item.StatusId = req.StatusId;
            }
            DBContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Room Status Updated Successfully"
            };
        }
    }
}
