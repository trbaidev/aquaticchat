﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mConfigurations;
using static Model.mGeneric;

namespace Provider.Repositories.ConfigurationsRepository
{
    public interface IConfigurationsRepository
    {
        mApiResponse GetConfigurationStatus(mGetConfigurationStatusReq req);
        mApiResponse EnableConversations(mEnableConversationsReq req);
        mApiResponse GetAllCampaigns(mGetAllCampaignsReq req);
        mApiResponse EnableCampaigns(List<mEnableCampaignsReq> req);
        mApiResponse AddUpdateCampaignSettings(mAddUpdateCampaignSettingsReq req);
        mApiResponse AddUpdateProductNotificationSettings(mAddUpdateProductNotificationSettingsReq req);
        mApiResponse GetCampaignSettingsByCampaignId(GetCampaignSettingsByCampaignIdReq req);
        mApiResponse GetAllCampaignsGrid(GetAllCampaignsGridReq req);
        mApiResponse GetConversationStatuses(GetConversationStatusesReq req);
        mApiResponse GetCampaignsddlWithCount(GetCampaignsddlWithCountReq req);
    }
}
