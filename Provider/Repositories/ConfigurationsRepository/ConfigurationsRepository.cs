﻿using DB.SQL.Context;
using DB.SQL.Context.Tables;
using Microsoft.EntityFrameworkCore;
using Provider.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.mConfigurations;
using static Model.mGeneric;

namespace Provider.Repositories.ConfigurationsRepository
{
    public class ConfigurationsRepository : IConfigurationsRepository
    {
        private ACDbContext dbContext;
        public ConfigurationsRepository(ACDbContext _dbContext)
        {
            dbContext = _dbContext;
        }
        public mApiResponse GetConfigurationStatus(mGetConfigurationStatusReq req)
        {
            Products_TB PT = dbContext.Products_TB.Where(x => x.Id == req.ProductId).FirstOrDefault();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseData = PT,
                ResponseMsg = "Success"
            };
        }
        public mApiResponse EnableConversations(mEnableConversationsReq req)
        {
            Products_TB PT = dbContext.Products_TB.Where(x => x.Id == req.ProductId).FirstOrDefault();
            if (PT != null)
            {
                PT.IsActive = req.IsActive;
                dbContext.SaveChanges();
                return new mApiResponse
                {
                    ResponseCode = 1,
                    ResponseMsg = "Conversations Enabled Successfully"
                };
            }
            else
            {
                return new mApiResponse
                {
                    ResponseCode = 0,
                    ResponseMsg = "Product Not Found"
                };
            }
        }
        public mApiResponse GetAllCampaigns(mGetAllCampaignsReq req)
        {
            List<mGetAllCampaignsResp> dbResp = dbContext.Campaigns_TB.Where(x => x.ProductId == req.ProductId && x.IsDefault == true)
                .Select(x => new mGetAllCampaignsResp
                {
                    Id = x.Id,
                    Name = x.Name,
                    Type = x.Type,
                    IsEnabled = x.IsEnabled,
                    Discription = x.Discription,
                    IsActive = x.IsActive
                }).ToList();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = dbResp
            };
        }
        public mApiResponse EnableCampaigns(List<mEnableCampaignsReq> req)
        {
            List<int> CampaignIds = req.Select(x => x.CampaignId).ToList();
            List<Campaigns_TB> dbResp = dbContext.Campaigns_TB.Where(x => CampaignIds.Contains(x.Id)).ToList();
            foreach (var item in dbResp)
            {
                bool isEnabled = req.Where(x => x.CampaignId == item.Id).Select(x => x.IsEnabled).FirstOrDefault();
                item.IsEnabled = isEnabled;
            }
            dbContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Campaigns Enabled Successfully"
            };
        }
        public mApiResponse AddUpdateCampaignSettings(mAddUpdateCampaignSettingsReq req)
        {
            if (req.CampaignId == 0) {
                Campaigns_TB CT = new Campaigns_TB
                {
                    CreatedOn = DateTime.Now,
                    Discription = req.ShortDescription,
                    Name = req.NamePublic,
                    IsActive = 1,
                    IsEnabled = true,
                    IsDefault = false,
                    ProductId = req.ProductId,
                    Type = ""
                };
                dbContext.Campaigns_TB.Add(CT);
                dbContext.SaveChanges();
                req.CampaignId = CT.Id;
            } else
            {
                Campaigns_TB CT = dbContext.Campaigns_TB.Where(x => x.Id == req.CampaignId).FirstOrDefault();
                CT.UpdatedOn = DateTime.Now;
                CT.Discription = req.ShortDescription;
                CT.Name = req.NamePublic;
                CT.IsActive = 1;
                CT.IsEnabled = true;
                CT.IsDefault = false;
                CT.ProductId = req.ProductId;
                dbContext.SaveChanges();
            }
            CampaignSettings_TB campaignSettings_TB = dbContext.CampaignSettings_TB.Where(x => x.CampaignId == req.CampaignId).FirstOrDefault();
            if (campaignSettings_TB == null)
            {
                campaignSettings_TB = new CampaignSettings_TB
                {
                    CampaignId = req.CampaignId,
                    Color = req.Color,
                    CreatedOn = DateTime.Now,
                    IsDeplayedInAdminInbox = req.IsDeplayedInAdminInbox,
                    IsGroupChat = req.IsGroupChat,
                    IsPublic = req.IsPublic,
                    NameInternal = req.NameInternal,
                    NamePublic = req.NamePublic,
                    ShortDescription = req.ShortDescription
                };
                dbContext.CampaignSettings_TB.Add(campaignSettings_TB);
            } else
            {
                campaignSettings_TB.CampaignId = req.CampaignId;
                campaignSettings_TB.Color = req.Color;
                campaignSettings_TB.CreatedOn = DateTime.Now;
                campaignSettings_TB.IsDeplayedInAdminInbox = req.IsDeplayedInAdminInbox;
                campaignSettings_TB.IsGroupChat = req.IsGroupChat;
                campaignSettings_TB.IsPublic = req.IsPublic;
                campaignSettings_TB.NameInternal = req.NameInternal;
                campaignSettings_TB.NamePublic = req.NamePublic;
                campaignSettings_TB.ShortDescription = req.ShortDescription;
            }
            dbContext.CampaignChannels_TB.RemoveRange(dbContext.CampaignChannels_TB.Where(x => x.CampaignId == req.CampaignId));
            List<CampaignChannels_TB> campaignChannels_TBs = req.Channels
                .Select(x => new CampaignChannels_TB
                {
                    CampaignId = req.CampaignId,
                    CreatedOn = DateTime.Now,
                    Name = x
                }).ToList();
            dbContext.CampaignChannels_TB.AddRange(campaignChannels_TBs);
            dbContext.CampaignPermissions_TB.RemoveRange(dbContext.CampaignPermissions_TB.Where(x => x.CampaignId == req.CampaignId));
            List<CampaignPermissions_TB> campaignPermissions_TBs = req.Permissions
                .SelectMany(x => x.Values.Select(y => new CampaignPermissions_TB
                {
                    CampaignId = req.CampaignId,
                    CreatedOn = DateTime.Now,
                    Name = x.Name,
                    Value = y
                })).ToList();
            dbContext.CampaignPermissions_TB.AddRange(campaignPermissions_TBs);
            dbContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Campaign Setting Added Successfully"
            };
        }

        public mApiResponse GetCampaignSettingsByCampaignId(GetCampaignSettingsByCampaignIdReq req)
        {
            GetCampaignSettingsResp dbResp = dbContext.Campaigns_TB.Where(x => x.Id == req.CampaignId)
                .Include(x => x.CampaignSettings_TB).Include(x => x.CampaignChannels_TBs)
                .Include(x => x.CampaignPermissions_TBs)
                .Select(x => new GetCampaignSettingsResp
                {
                    Color = x.CampaignSettings_TB.Color,
                    IsDeplayedInAdminInbox = x.CampaignSettings_TB.IsDeplayedInAdminInbox,
                    IsGroupChat = x.CampaignSettings_TB.IsGroupChat,
                    IsPublic = x.CampaignSettings_TB.IsPublic,
                    NameInternal = x.CampaignSettings_TB.NameInternal,
                    NamePublic = x.CampaignSettings_TB.NamePublic,
                    ShortDescription = x.CampaignSettings_TB.ShortDescription,
                    Channels = x.CampaignChannels_TBs.Select(x => x.Name).ToList(),
                }).FirstOrDefault();
            dbResp.Permissions = dbContext.CampaignPermissions_TB.Where(x => x.CampaignId == req.CampaignId).ToList().GroupBy(x => x.Name).Select(x => new mConversationPermissions
            {
                Name = x.Key,
                Values = x.Select(x => x.Value).ToList()
            }).ToList();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = dbResp
            };
        }

        public mApiResponse AddUpdateProductNotificationSettings(mAddUpdateProductNotificationSettingsReq req)
        {
            if(req.ProductNotificationSettings.Count > 0)
            {
                dbContext.NotificationsSettings_TB.RemoveRange(dbContext.NotificationsSettings_TB
                    .Where(x => x.ProductId == req.ProductId && x.HubUserId == null).ToList()
                    );
                List<NotificationsSettings_TB> ProductNotifications = new List<NotificationsSettings_TB>();
                foreach (mCampaignNotificationsSettings item in req.ProductNotificationSettings)
                {
                    ProductNotifications.Add(new NotificationsSettings_TB
                    {
                        ProductId = req.ProductId,
                        CampaignId = item.CampaignId,
                        IsGlobal = item.IsGlobal,
                        CreatedOn = DateTime.Now
                    });
                }
                dbContext.NotificationsSettings_TB.AddRange(ProductNotifications);
            }
            if(req.UserNotificationSettings.Count > 0)
            {
                dbContext.NotificationsSettings_TB.RemoveRange(dbContext.NotificationsSettings_TB
                   .Where(x => x.ProductId == req.ProductId && x.HubUserId == req.HubUserId).ToList()
                   );
                List<NotificationsSettings_TB> ProductNotifications = new List<NotificationsSettings_TB>();
                foreach (mCampaignNotificationsSettings item in req.ProductNotificationSettings)
                {
                    ProductNotifications.Add(new NotificationsSettings_TB
                    {
                        ProductId = req.ProductId,
                        CampaignId = item.CampaignId,
                        HubUserId = req.HubUserId,
                        IsGlobal = item.IsGlobal,
                        CreatedOn = DateTime.Now
                    });
                }
                dbContext.NotificationsSettings_TB.AddRange(ProductNotifications);
            }
            dbContext.SaveChanges();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Notifications Setting Updated Successfully"
            };
        }
        public mApiResponse GetAllCampaignsGrid(GetAllCampaignsGridReq req)
        {
            var query = dbContext.Campaigns_TB.Where(x => x.ProductId == req.ProductId);
            if (!string.IsNullOrEmpty(req.CampaignName))
            {
                query = query.Where(x => x.Name.Contains(req.CampaignName));
            }
            List<GetAllCampaignsGridDbResp> dbResp = query.Select(x => new GetAllCampaignsGridDbResp
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                IsDefault = x.IsDefault,
                ConversationSize = x.CampaignSettings_TB.IsGroupChat ? "1:1 + Group Chat" : "1:1 Chat",
                Eligibility = x.CampaignPermissions_TBs.Select(x => x.Name).ToList()
            }).OrderByCustom(string.IsNullOrEmpty(req.Pagination.OrderByKey) ? "Id" : req.Pagination.OrderByKey,
                    req.Pagination.OrderByVal == 1 ? true : false)
                .Skip((req.Pagination.PageNo - 1) * req.Pagination.PageSize).Take(req.Pagination.PageSize)
                .ToList();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = new GetAllCampaignsGridResp
                {
                    Data = dbResp,
                    Pagination = new mPagingBasicResp
                    {
                        CurrentCount = dbResp.Count,
                        TotalRecords = query.Count()
                    }
                }
            };

        }
        public mApiResponse GetConversationStatuses(GetConversationStatusesReq req)
        {
            var query = dbContext.ConversationStatus_TB.Where(x => x.ProductId == req.ProductId);
            if(!string.IsNullOrEmpty(req.StatusName))
            {
                query = query.Where(x => x.NameInternal.Contains(req.StatusName));
            }
            List<GetConversationStatusesResp> dbresp = query.Select(x => new GetConversationStatusesResp
            {
                Id = x.Id,
                NameInternal =  x.NameInternal,
                Action = x.Action,
                IsDefault = x.IsDefault
            }).ToList();
            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = dbresp
            };
        }
        public mApiResponse GetCampaignsddlWithCount(GetCampaignsddlWithCountReq req)
        {
            List<mDropDown> CampaignsData = new List<mDropDown>();
            mDropDown AllChats = new mDropDown
            {
                Id = "All",
                Value = "All",
                Generic = dbContext.Rooms_TB.Where(x => x.ProductId == req.ProductId).Count()
            };
            CampaignsData.Add(AllChats);
            mDropDown YourBrandChats = new mDropDown
            {
                Id = "YourBrand",
                Value = "Your Brand",
                Generic = dbContext.UserRoom_TB.Where(x => x.UserId == req.HubUserId && x.IsActive == 1).Count()
            };
            CampaignsData.Add(YourBrandChats);
            mDropDown FavouriteChats = new mDropDown
            {
                Id = "Favorite",
                Value = "Favorite",
                Generic = dbContext.UserRoom_TB.Where(x => x.UserId == req.HubUserId && x.Is_Favourite == true && x.IsActive == 1).Count()
            };
            CampaignsData.Add(FavouriteChats);
            CampaignsData.AddRange(dbContext.Campaigns_TB
                .Where(x => x.IsActive == 1 && x.IsEnabled == true)
                .Select(x => new mDropDown
                {
                    Id = x.Id.ToString(),
                    Value = x.Name.ToString(),
                    Generic = x.Rooms_TBs.Where(x => x.IsActive == 1).Count()
                }).ToList());


            return new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = CampaignsData
            };
        }
    }
}
