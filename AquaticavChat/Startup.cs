using AquaticavChat.Auth;
using AquaticavChat.Chat;
using DB.SQL.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Provider.Repositories.ChatHubRepository;
using Provider.Repositories.ConfigurationsRepository;
using Provider.Repositories.GenericRepository;
using Provider.Repositories.HubRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AquaticavChat
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ACDbContext>
                (item => item.UseSqlServer(Configuration.GetConnectionString("SQLConnection")));
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                          builder =>
                          {
                              builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .AllowAnyMethod()
                    .WithOrigins("http://localhost:4200", 
                            "https://hottubtoolsstagging.azurewebsites.net", 
                            "http://localhost:3000", 
                            "https://aavofishstaging.azurewebsites.net", 
                            "https://accountstagging.azurewebsites.net",
                            "https://aavofishdev.azurewebsites.net",
                            "https://accountsdev.azurewebsites.net");
                          });
            });
            // Add SignalR  
            services.AddSingleton<IAuthorizationHandler, CustomAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("CustomHubAuthorizatioPolicy", policy =>
                {
                    policy.Requirements.Add(new CustomAuthorizationRequirement());
                });
            });
            
            services.AddMvc();
            services.AddControllers();
            services.AddHttpContextAccessor();
            // Http context accessor singleton  
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            
            services.AddSingleton<IUserIdProvider, CustomUserIdProvider>();
            #region Repositories
            services.AddTransient<IChatHubRepository, ChatHubRepository>();
            services.AddTransient<IGenericRepository, GenericRepository>();
            services.AddTransient<IHubRepository, HubRepository>();
            services.AddTransient<IConfigurationsRepository, ConfigurationsRepository>();
            #endregion
            services.AddSignalR().AddMessagePackProtocol();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AquaticavChat", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowAllOrigins");
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AquaticavChat v1"));
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/signalr");
            });
        }
    }
}
