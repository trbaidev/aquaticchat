﻿using AquaticavChat.Chat;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Model;
using Provider.Repositories.ChatHubRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Provider.Util;
using static Model.mChatHub;
using Microsoft.AspNetCore.Http.Features;
using static Model.mGeneric;
using static Model.mHub;

namespace AquaticavChat.Chat
{
    [Authorize(Policy = "CustomHubAuthorizatioPolicy")]
    public class ChatHub : Microsoft.AspNetCore.SignalR.Hub<IChatHub>
    {
        IChatHubRepository Repo;
        
        public ChatHub(IChatHubRepository _Repo)
        {
            Repo = _Repo;
        }
        //public async Task BroadcastAsync(mChatMessage message)
        //{
        //    List<string> list = new List<string>() { 
        //        ""
        //    };
        //    await Clients.All.MessageReceivedFromHub(message);
        //    //.Users(list).MessageReceivedFromHub(message);//
        //}

        public async Task SendMessage(SendMessage Req)
        {
            Tuple<List<string>, MessageChat> Resp = Repo.SaveMessageAndGetConnectionIds(Req);
            await Clients.Clients(Resp.Item1).MessageRecieved(Resp.Item2);
        }

        public override async Task OnConnectedAsync()
        {
            var realIP = Context.GetHttpContext().Request.Headers["X-Real-IP"];
            var feature = Context.Features.Get<IHttpConnectionFeature>();
            OnConnected OC = new OnConnected
            {
                ClientId = Context.GetHttpContext().Request.Query["ClientId"].ToString(),
                ConnectionId = Context.ConnectionId,
                ClientName = Context.GetHttpContext().Request.Query["ClientName"].ToString(),
                ProductId = Context.GetHttpContext().Request.Query["ProductId"].ToString().toInt(),
                Generic = Context.GetHttpContext().Request.Query["Generic"].ToString(),
                Ip = "123"//feature.RemoteIpAddress.Address.ToString()

            };
            int HubUserId = Repo.GetHubUserId(OC);
            await Clients.Client(Context.ConnectionId).onUserConnected(new mOnUserConnected { HubUserId = HubUserId });
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Repo.RemoveUserConnection(Context.ConnectionId);
        }
    }
    
    
}
