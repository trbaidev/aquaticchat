﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Model.mHub;

namespace AquaticavChat.Chat
{
    public interface IChatHub
    {
        //Task MessageReceivedFromHub(mChatMessage message);

        //Task NewUserConnected(string message);

        Task onUserConnected(mOnUserConnected mOnUserConnected);

        Task MessageRecieved(MessageChat Message);
    }
}
