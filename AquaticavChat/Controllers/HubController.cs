﻿using AquaticavChat.Chat;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Provider.Repositories.ChatHubRepository;
using Provider.Repositories.HubRepository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static Model.mChatHub;
using static Model.mGeneric;
using static Model.mHub;

namespace AquaticavChat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HubController : ControllerBase
    {
        IHubRepository Repo;
        private readonly IHubContext<ChatHub> HubContext;
        public HubController(IHubRepository _Repo, IHubContext<ChatHub> _HubContext)
        {
            Repo = _Repo;
            HubContext = _HubContext;
        }
        [HttpPost("CreateRoom")]
        public ActionResult CreateRoom(CreateRoomReq Req)
        {
            mApiResponse Resp = new mApiResponse {
                ResponseCode = 1,
                ResponseData = Repo.CreateRoom(Req),
                ResponseMsg = "Room Created Successfully"
            };
            return Ok(Resp);
        }
        [HttpPost("DeleteRooms")]
        public ActionResult DeleteRooms(DeleteRoomsReq Req)
        {
            mApiResponse Resp = Repo.DeleteRooms(Req);
            return Ok(Resp);
        }
        [HttpPost("SendMsgViaAPI")]
        public async Task<ActionResult> SendMsgViaAPI(SendMsgViaAPIReq Req)
        {
            Tuple<List<string>, MessageChat> Resp = Repo.SendMsgViaAPI(Req);
            List<string> Connectionids = Resp.Item1;
            await HubContext.Clients.Clients(Connectionids).SendAsync("MessageRecieved", Resp.Item2);
            var GreetingResp = SendGreetingMessage(new SendGreetingMessageReq
            {
                HubUserId = Resp.Item2.HubUserId,
                ProductId = Req.ProductId,
                RoomId = Resp.Item2.RoomId,
                AllowedOnCampaignsId = Req.MessageFrom.AllowedOnCampaignsId
            });
            
            return Ok(new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Message Sent Successfully",
                ResponseData = Resp.Item2
            });
        }
        [HttpPost("SendGreetingMessage")]
        public async Task<bool> SendGreetingMessage(SendGreetingMessageReq req)
        {
            Tuple<List<string>, MessageChat> CampaignMessage = Repo.SendGreetingMessage(req);
            await Task.Delay(1000);
            
            await HubContext.Clients.Clients(CampaignMessage.Item1).SendAsync("MessageRecieved", CampaignMessage.Item2);
            return true;
        }
        [HttpPost("UpdateCampaignIdInRoom")]
        public async Task<ActionResult> UpdateCampaignIdInRoom(UpdateCampaignIdInRoomReq req)
        {
            Tuple<List<string>, MessageChat> resp = Repo.UpdateCampaignIdInRoom(req);
            await HubContext.Clients.Clients(resp.Item1).SendAsync("MessageRecieved", resp.Item2);
            return Ok(new mApiResponse
            {
                 ResponseCode = 1,
                 ResponseMsg = "CampaignId Updated Successfully",
                 ResponseData = resp.Item2
            });
        }
        [HttpPost("GetCampaignsDdl")]
        public ActionResult GetCampaignsDdl(GetCampaignsDdlReq req)
        {
            List<mDropDown> resp = Repo.GetCampaignsDdl(req);
            return Ok(new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Success",
                ResponseData = resp
            });
        }
        [HttpPost("GetUserRoomsByUsersId")]
        public ActionResult GetUserRoomsByUsersId(GetUserRoomsByUsersIdReq Req)
        {
            mApiResponse Resp = Repo.GetUserRoomsByUsersId(Req);
            return Ok(Resp);
        }
        [HttpPost("GetChatDetailsByRoomId")]
        public ActionResult GetChatDetailsByRoomId(GetChatDetailsByRoomIdReq Req)
        {
            mApiResponse Resp = Repo.GetChatDetailsByRoomId(Req);
            return Ok(Resp);
        }
        [HttpPost("MakeRoomFavorite")]
        public ActionResult MakeRoomFavorite(MakeRoomFavoriteReq Req)
        {
            mApiResponse Resp = Repo.MakeRoomFavorite(Req);
            return Ok(Resp);
        }
        [HttpPost("MakeRoomFavoriteBulk")]
        public ActionResult MakeRoomFavoriteBulk(MakeRoomFavoriteBulkReq Req)
        {
            mApiResponse Resp = Repo.MakeRoomFavoriteBulk(Req);
            return Ok(Resp);
        }
        [HttpPost("UpdateConversationStatusInRoom")]
        public ActionResult UpdateConversationStatusInRoom(UpdateConversationStatusInRoom Req)
        {
            mApiResponse resp = Repo.UpdateConversationStatusInRoom(Req);
            return Ok(resp);
        }
        [HttpPost("UploadFile")]
        public ActionResult UploadFile(IFormCollection formdata)
        {
            var file = formdata.Files[0];
            var Extension = Path.GetExtension(file.FileName);
            string fileName = Guid.NewGuid().ToString().Replace("-", "");
            var path = Path.Combine("Uploads", "Chats");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string FilePath = Path.Combine(path, fileName + "" + Extension);
            using (FileStream stream = new FileStream(FilePath, FileMode.Create))
            {
                file.CopyTo(stream);

            }
            return Ok(new mApiResponse
            {
                ResponseCode = 1,
                ResponseMsg = "Image Uploaded Successfully",
                ResponseData = FilePath
            });
        }
        [HttpGet("DownloadFile/{Path}")]
        public ActionResult DownloadFile(string Path)
        {
            Path = string.Join("\\", Path.Split("||"));
            var TempFile = System.IO.File.OpenRead(Path);
            string mimeType = GetMimeType(TempFile.Name);
            return File(TempFile, mimeType);
        }
        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
    }
}
