﻿using Microsoft.AspNetCore.Mvc;
using Provider.Repositories.ConfigurationsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Model.mConfigurations;
using static Model.mGeneric;

namespace AquaticavChat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationsController : ControllerBase
    {
        IConfigurationsRepository repo;
        public ConfigurationsController(IConfigurationsRepository _repo)
        {
            repo = _repo;
        }
        [HttpPost("GetConfigurationStatus")]
        public IActionResult GetConfigurationStatus(mGetConfigurationStatusReq req)
        {
            mApiResponse resp = repo.GetConfigurationStatus(req);
            return Ok(resp);
        }
        [HttpPost("EnableConversations")]
        public IActionResult EnableConversations(mEnableConversationsReq req)
        {
            mApiResponse resp = repo.EnableConversations(req);
            return Ok(resp);
        }
        [HttpPost("GetAllCampaigns")]
        public IActionResult GetAllCampaigns(mGetAllCampaignsReq req)
        {
            mApiResponse resp = repo.GetAllCampaigns(req);
            return Ok(resp);
        }
        [HttpPost("EnableCampaigns")]
        public IActionResult EnableCampaigns(List<mEnableCampaignsReq> req)
        {
            mApiResponse resp = repo.EnableCampaigns(req);
            return Ok(resp);
        }
        [HttpPost("AddUpdateCampaignSettings")]
        public IActionResult AddUpdateCampaignSettings(mAddUpdateCampaignSettingsReq req)
        {
            mApiResponse resp = repo.AddUpdateCampaignSettings(req);
            return Ok(resp);
        }
        [HttpPost("GetCampaignSettingsByCampaignId")]
        public IActionResult GetCampaignSettings(GetCampaignSettingsByCampaignIdReq req)
        {
            mApiResponse resp = repo.GetCampaignSettingsByCampaignId(req);
            return Ok(resp);
        }
        [HttpPost("AddUpdateProductNotificationSettings")]
        public IActionResult AddUpdateProductNotificationSettings(mAddUpdateProductNotificationSettingsReq req)
        {
            mApiResponse resp = repo.AddUpdateProductNotificationSettings(req);
            return Ok(resp);
        }
        [HttpPost("GetAllCampaignsGrid")]
        public IActionResult GetAllCampaignsGrid(GetAllCampaignsGridReq req)
        {
            mApiResponse resp = repo.GetAllCampaignsGrid(req);
            return Ok(resp);
        }
        [HttpPost("GetConversationStatuses")]
        public IActionResult GetConversationStatuses(GetConversationStatusesReq req)
        {
            mApiResponse resp = repo.GetConversationStatuses(req);
            return Ok(resp);
        }
        [HttpPost("GetCampaignsddlWithCount")]
        public IActionResult GetCampaignsddlWithCount(GetCampaignsddlWithCountReq req)
        {
            mApiResponse resp = repo.GetCampaignsddlWithCount(req);
            return Ok(resp);
        }
    }
}
