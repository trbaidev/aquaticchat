﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedNewColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Is_Favourite",
                table: "UserRoom_TB",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 21, 31, 57, 239, DateTimeKind.Local).AddTicks(372));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 21, 31, 57, 240, DateTimeKind.Local).AddTicks(4965));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 21, 31, 57, 240, DateTimeKind.Local).AddTicks(5001));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Is_Favourite",
                table: "UserRoom_TB");

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 1, 8, 8, 713, DateTimeKind.Local).AddTicks(9441));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 1, 8, 8, 715, DateTimeKind.Local).AddTicks(2272));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 10, 29, 1, 8, 8, 715, DateTimeKind.Local).AddTicks(2296));
        }
    }
}
