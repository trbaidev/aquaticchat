﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedIsDefaultInCampaigns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Campaigns_TB",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "IsDefault" },
                values: new object[] { new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(2213), true });

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "IsDefault" },
                values: new object[] { new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4411), true });

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "IsDefault" },
                values: new object[] { new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4425), true });

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "IsDefault" },
                values: new object[] { new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4430), true });

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedOn", "IsDefault" },
                values: new object[] { new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4453), true });

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 124, DateTimeKind.Local).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 126, DateTimeKind.Local).AddTicks(2479));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 126, DateTimeKind.Local).AddTicks(3538));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Campaigns_TB");

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(3976));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4899));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4909));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4913));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4917));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 119, DateTimeKind.Local).AddTicks(702));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 120, DateTimeKind.Local).AddTicks(3904));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 120, DateTimeKind.Local).AddTicks(4777));
        }
    }
}
