﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedCampaignIdInRoomsTB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CampaignId",
                table: "Rooms_TB",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(3976));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4899));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4909));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4913));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 126, DateTimeKind.Local).AddTicks(4917));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 119, DateTimeKind.Local).AddTicks(702));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 120, DateTimeKind.Local).AddTicks(3904));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 23, 2, 32, 120, DateTimeKind.Local).AddTicks(4777));

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_TB_CampaignId",
                table: "Rooms_TB",
                column: "CampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_TB_Campaigns_TB_CampaignId",
                table: "Rooms_TB",
                column: "CampaignId",
                principalTable: "Campaigns_TB",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_TB_Campaigns_TB_CampaignId",
                table: "Rooms_TB");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_TB_CampaignId",
                table: "Rooms_TB");

            migrationBuilder.DropColumn(
                name: "CampaignId",
                table: "Rooms_TB");

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9041));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9065));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9071));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9075));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 362, DateTimeKind.Local).AddTicks(3734));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 364, DateTimeKind.Local).AddTicks(6608));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 364, DateTimeKind.Local).AddTicks(8202));
        }
    }
}
