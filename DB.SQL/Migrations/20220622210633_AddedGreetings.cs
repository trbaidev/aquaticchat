﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedGreetings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GreetingMsg",
                table: "Products_TB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GreetingMsgFrom",
                table: "Products_TB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Messages_TB",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9041));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9065));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9071));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 372, DateTimeKind.Local).AddTicks(9075));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 362, DateTimeKind.Local).AddTicks(3734));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "GreetingMsg", "GreetingMsgFrom" },
                values: new object[] { new DateTime(2022, 6, 23, 2, 6, 32, 364, DateTimeKind.Local).AddTicks(6608), "Hi #CustomerName#, thanks for reaching out to Aquatic AV! How can we help you?", "AAV Bot" });

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 23, 2, 6, 32, 364, DateTimeKind.Local).AddTicks(8202));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GreetingMsg",
                table: "Products_TB");

            migrationBuilder.DropColumn(
                name: "GreetingMsgFrom",
                table: "Products_TB");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Messages_TB",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(1454));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2638));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2651));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2654));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2657));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 658, DateTimeKind.Local).AddTicks(2388));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 659, DateTimeKind.Local).AddTicks(8083));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 659, DateTimeKind.Local).AddTicks(8128));
        }
    }
}
