﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedConfigurations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsConfigured",
                table: "Products_TB",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Campaigns_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Discription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Campaigns_TB_Products_TB_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampaignChannels_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CampaignId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignChannels_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampaignChannels_TB_Campaigns_TB_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampaignPermissions_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CampaignId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignPermissions_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampaignPermissions_TB_Campaigns_TB_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CampaignSettings_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CampaignId = table.Column<int>(type: "int", nullable: false),
                    NameInternal = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NamePublic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Color = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShortDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsPublic = table.Column<bool>(type: "bit", nullable: false),
                    IsGroupChat = table.Column<bool>(type: "bit", nullable: false),
                    IsDeplayedInAdminInbox = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignSettings_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampaignSettings_TB_Campaigns_TB_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NotificationsSettings_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    CampaignId = table.Column<int>(type: "int", nullable: true),
                    HubUserId = table.Column<int>(type: "int", nullable: true),
                    IsGlobal = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationsSettings_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NotificationsSettings_TB_Campaigns_TB_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NotificationsSettings_TB_Products_TB_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NotificationsSettings_TB_Users_TB_HubUserId",
                        column: x => x.HubUserId,
                        principalTable: "Users_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Campaigns_TB",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "Discription", "Image", "IsActive", "IsEnabled", "Name", "ProductId", "Type", "UpdatedBy", "UpdatedOn" },
                values: new object[,]
                {
                    { 1, null, new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(1454), "General Conversations are started without following customer journeys or when a customer sends an e-mail to a address you’ve assigned to general inquires for OFish.", null, 1, false, "General Conversations", 2, "OFish Preset", null, null },
                    { 2, null, new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2638), "Press Conversations are started when members of the press/media submit requests from your OFish Chatbot or if they have an account in the newsroom.", null, 1, false, "Press Conversations", 2, "OFish Preset", null, null },
                    { 3, null, new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2651), "Facebook Messages will first be shown as a General Conversation until the conversation is manually changed by an OFish Brand Backend User", null, 0, false, "Facebook Conversations", 2, "", null, null },
                    { 4, null, new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2654), "Support Conversations are started when customers are reaching out for support. By Default customer can receive support via your Chatbot, in-app messaging, or e-mail.", null, 1, false, "Support Conversations ", 2, "OFish Preset", null, null },
                    { 5, null, new DateTime(2022, 6, 14, 0, 4, 28, 666, DateTimeKind.Local).AddTicks(2657), "Sales Conversations are started when leads and unknown users reach out via your chatbot or web forms", null, 1, false, "Sales Conversations", 2, "OFish Preset", null, null }
                });

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 658, DateTimeKind.Local).AddTicks(2388));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 659, DateTimeKind.Local).AddTicks(8083));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 14, 0, 4, 28, 659, DateTimeKind.Local).AddTicks(8128));

            migrationBuilder.CreateIndex(
                name: "IX_CampaignChannels_TB_CampaignId",
                table: "CampaignChannels_TB",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignPermissions_TB_CampaignId",
                table: "CampaignPermissions_TB",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_TB_ProductId",
                table: "Campaigns_TB",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignSettings_TB_CampaignId",
                table: "CampaignSettings_TB",
                column: "CampaignId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NotificationsSettings_TB_CampaignId",
                table: "NotificationsSettings_TB",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationsSettings_TB_HubUserId",
                table: "NotificationsSettings_TB",
                column: "HubUserId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationsSettings_TB_ProductId",
                table: "NotificationsSettings_TB",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CampaignChannels_TB");

            migrationBuilder.DropTable(
                name: "CampaignPermissions_TB");

            migrationBuilder.DropTable(
                name: "CampaignSettings_TB");

            migrationBuilder.DropTable(
                name: "NotificationsSettings_TB");

            migrationBuilder.DropTable(
                name: "Campaigns_TB");

            migrationBuilder.DropColumn(
                name: "IsConfigured",
                table: "Products_TB");

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 169, DateTimeKind.Local).AddTicks(4256));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 170, DateTimeKind.Local).AddTicks(8314));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 170, DateTimeKind.Local).AddTicks(8347));
        }
    }
}
