﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddedConversationStatusTB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Rooms_TB",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ConversationStatus_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    CampaignId = table.Column<int>(type: "int", nullable: true),
                    NameInternal = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NamePublic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Action = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Color = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    IsDefaultForNewMsgs = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConversationStatus_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConversationStatus_TB_Campaigns_TB_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ConversationStatus_TB_Products_TB_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 178, DateTimeKind.Local).AddTicks(1092));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 178, DateTimeKind.Local).AddTicks(2370));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 178, DateTimeKind.Local).AddTicks(2380));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 178, DateTimeKind.Local).AddTicks(2383));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 178, DateTimeKind.Local).AddTicks(2386));

            migrationBuilder.InsertData(
                table: "ConversationStatus_TB",
                columns: new[] { "Id", "Action", "CampaignId", "Color", "CreatedBy", "CreatedOn", "IsActive", "IsDefault", "IsDefaultForNewMsgs", "NameInternal", "NamePublic", "ProductId", "UpdatedBy", "UpdatedOn" },
                values: new object[,]
                {
                    { 1, "Leaves Conversation Open for Replies", null, "#6fcf97", null, new DateTime(2022, 6, 29, 23, 37, 9, 191, DateTimeKind.Local).AddTicks(6503), true, true, true, "Open", "Open", 2, null, null },
                    { 2, "Conversations Not Yet Sent", null, "#e0e0e0", null, new DateTime(2022, 6, 29, 23, 37, 9, 191, DateTimeKind.Local).AddTicks(7699), true, true, false, "Draft", "Draft", 2, null, null },
                    { 3, "Closes the Conversation + Prevents Customer Replies unless Re-Opened", null, "#4f4f4f", null, new DateTime(2022, 6, 29, 23, 37, 9, 191, DateTimeKind.Local).AddTicks(7709), true, true, false, "Closed", "Closed", 2, null, null },
                    { 4, "Once a Conversation has been closed for OFish will Archive it from your Main Inbox.", null, "#4f4f4f", null, new DateTime(2022, 6, 29, 23, 37, 9, 191, DateTimeKind.Local).AddTicks(7712), true, true, false, "Closed & Archived", "Closed & Archived", 2, null, null }
                });

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 170, DateTimeKind.Local).AddTicks(9114));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 172, DateTimeKind.Local).AddTicks(1962));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 23, 37, 9, 172, DateTimeKind.Local).AddTicks(2943));

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_TB_StatusId",
                table: "Rooms_TB",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ConversationStatus_TB_CampaignId",
                table: "ConversationStatus_TB",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ConversationStatus_TB_ProductId",
                table: "ConversationStatus_TB",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rooms_TB_ConversationStatus_TB_StatusId",
                table: "Rooms_TB",
                column: "StatusId",
                principalTable: "ConversationStatus_TB",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rooms_TB_ConversationStatus_TB_StatusId",
                table: "Rooms_TB");

            migrationBuilder.DropTable(
                name: "ConversationStatus_TB");

            migrationBuilder.DropIndex(
                name: "IX_Rooms_TB_StatusId",
                table: "Rooms_TB");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Rooms_TB");

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(2213));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4411));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4425));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4430));

            migrationBuilder.UpdateData(
                table: "Campaigns_TB",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 134, DateTimeKind.Local).AddTicks(4453));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 124, DateTimeKind.Local).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 126, DateTimeKind.Local).AddTicks(2479));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2022, 6, 29, 22, 22, 52, 126, DateTimeKind.Local).AddTicks(3538));
        }
    }
}
