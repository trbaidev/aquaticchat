﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class DefaultDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 169, DateTimeKind.Local).AddTicks(4256));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 170, DateTimeKind.Local).AddTicks(8314));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 18, 10, 52, 170, DateTimeKind.Local).AddTicks(8347));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 17, 41, 24, 945, DateTimeKind.Local).AddTicks(549));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 17, 41, 24, 946, DateTimeKind.Local).AddTicks(9188));

            migrationBuilder.UpdateData(
                table: "Products_TB",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2021, 12, 10, 17, 41, 24, 946, DateTimeKind.Local).AddTicks(9230));
        }
    }
}
