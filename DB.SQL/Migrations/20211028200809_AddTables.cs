﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.SQL.Migrations
{
    public partial class AddTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products_TB", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rooms_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    ProductId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rooms_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rooms_TB_Products_TB_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClientName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Generic = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_TB_Products_TB_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Messages_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    RoomId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Messages_TB_Rooms_TB_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Rooms_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Messages_TB_Users_TB_UserId",
                        column: x => x.UserId,
                        principalTable: "Users_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserConnection_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConnectionId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ip = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserConnection_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserConnection_TB_Users_TB_UserId",
                        column: x => x.UserId,
                        principalTable: "Users_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRoom_TB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<int>(type: "int", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoomId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoom_TB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoom_TB_Rooms_TB_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Rooms_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRoom_TB_Users_TB_UserId",
                        column: x => x.UserId,
                        principalTable: "Users_TB",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Products_TB",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsActive", "Name", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 1, null, new DateTime(2021, 10, 29, 1, 8, 8, 713, DateTimeKind.Local).AddTicks(9441), 1, "HotTub", null, null });

            migrationBuilder.InsertData(
                table: "Products_TB",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsActive", "Name", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 2, null, new DateTime(2021, 10, 29, 1, 8, 8, 715, DateTimeKind.Local).AddTicks(2272), 1, "OFish", null, null });

            migrationBuilder.InsertData(
                table: "Products_TB",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsActive", "Name", "UpdatedBy", "UpdatedOn" },
                values: new object[] { 3, null, new DateTime(2021, 10, 29, 1, 8, 8, 715, DateTimeKind.Local).AddTicks(2296), 1, "RGB", null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Messages_TB_RoomId",
                table: "Messages_TB",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_TB_UserId",
                table: "Messages_TB",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Rooms_TB_ProductId",
                table: "Rooms_TB",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_UserConnection_TB_UserId",
                table: "UserConnection_TB",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoom_TB_RoomId",
                table: "UserRoom_TB",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoom_TB_UserId",
                table: "UserRoom_TB",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TB_ProductId",
                table: "Users_TB",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Messages_TB");

            migrationBuilder.DropTable(
                name: "UserConnection_TB");

            migrationBuilder.DropTable(
                name: "UserRoom_TB");

            migrationBuilder.DropTable(
                name: "Rooms_TB");

            migrationBuilder.DropTable(
                name: "Users_TB");

            migrationBuilder.DropTable(
                name: "Products_TB");
        }
    }
}
