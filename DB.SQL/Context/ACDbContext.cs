﻿using DB.SQL.Context.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context
{
    public class ACDbContext : DbContext
    {
        public ACDbContext(DbContextOptions<ACDbContext> options) : base(options)
        {
        }
        public DbSet<Messages_TB> Messages_TB { get; set; }
        public DbSet<Products_TB> Products_TB { get; set; }
        public DbSet<Users_TB> Users_TB { get; set; }
        public DbSet<UserConnection_TB> UserConnection_TB { get; set; }
        public DbSet<Rooms_TB> Rooms_TB { get; set; }
        public DbSet<UserRoom_TB> UserRoom_TB { get; set; }
        public DbSet<MessageAttachments_TB> MessageAttachments_TB { get; set; }
        public DbSet<Campaigns_TB> Campaigns_TB { get; set; }
        public DbSet<CampaignSettings_TB> CampaignSettings_TB { get; set; }
        public DbSet<CampaignChannels_TB> CampaignChannels_TB { get; set; }
        public DbSet<CampaignPermissions_TB> CampaignPermissions_TB { get; set; }
        public DbSet<NotificationsSettings_TB> NotificationsSettings_TB { get; set; }
        public DbSet<ConversationStatus_TB> ConversationStatus_TB { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Messages_TB
            modelBuilder.Entity<Messages_TB>()
                .HasOne(x => x.Rooms_TB)
                .WithMany(x => x.Messages_TBs)
                .HasForeignKey(x => x.RoomId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Messages_TB>()
                .HasOne(x => x.Users_TB)
                .WithMany(x => x.Messages_TBs)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region MessageAttachments_TB
            modelBuilder.Entity<MessageAttachments_TB>()
                .HasOne(x => x.Messages_TB)
                .WithMany(x => x.MessageAttachments_TB)
                .HasForeignKey(x => x.MessageId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Products_TB
            modelBuilder.Entity<Products_TB>().HasData(
                new Products_TB { Id = 1, Name = "HotTub", CreatedOn = DateTime.Now, IsActive = 1 },
                new Products_TB { Id = 2, Name = "OFish", CreatedOn = DateTime.Now, IsActive = 1 , GreetingMsg = "Hi #CustomerName#, thanks for reaching out to Aquatic AV! How can we help you?" , GreetingMsgFrom = "AAV Bot" },
                new Products_TB { Id = 3, Name = "RGB", CreatedOn = DateTime.Now, IsActive = 1 }
            );
            #endregion
            #region Rooms_TB
            modelBuilder.Entity<Rooms_TB>()
                .HasOne(x => x.Products_TB)
                .WithMany(x => x.Rooms_TBs)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Rooms_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithMany(x => x.Rooms_TBs)
                .HasForeignKey(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Rooms_TB>()
                .HasOne(x => x.ConversationStatus_TB)
                .WithMany(x => x.Rooms_TBs)
                .HasForeignKey(x => x.StatusId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Users_TB
            modelBuilder.Entity<Users_TB>()
                .HasOne(x => x.Products_TB)
                .WithMany(x => x.Users_TBs)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion
            #region UserConnection_TB
            modelBuilder.Entity<UserConnection_TB>()
                .HasOne(x => x.Users_TB)
                .WithMany(x => x.UserConnection_TBs)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region UserRoom_TB
            modelBuilder.Entity<UserRoom_TB>()
                .HasOne(x => x.Users_TB)
                .WithMany(x => x.UserRoom_TBs)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserRoom_TB>()
                .HasOne(x => x.Rooms_TB)
                .WithMany(x => x.UserRoom_TBs)
                .HasForeignKey(x => x.RoomId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Campaigns
            modelBuilder.Entity<Campaigns_TB>()
                .HasOne(x => x.Products_TB)
                .WithMany(x => x.Campaigns_TBs)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Campaigns_TB>().HasData(
                new Campaigns_TB { Id = 1, Name = "General Conversations",ProductId = 2, Type = "OFish Preset", IsEnabled = false, Discription= "General Conversations are started without following customer journeys or when a customer sends an e-mail to a address you’ve assigned to general inquires for OFish.",  CreatedOn = DateTime.Now, IsActive = 1, IsDefault = true },
                new Campaigns_TB { Id = 2, Name = "Press Conversations", ProductId = 2, Type = "OFish Preset", IsEnabled = false, Discription = "Press Conversations are started when members of the press/media submit requests from your OFish Chatbot or if they have an account in the newsroom.", CreatedOn = DateTime.Now, IsActive = 1 , IsDefault = true },
                new Campaigns_TB { Id = 3, Name = "Facebook Conversations", ProductId = 2, Type = "", IsEnabled = false, Discription = "Facebook Messages will first be shown as a General Conversation until the conversation is manually changed by an OFish Brand Backend User", CreatedOn = DateTime.Now, IsActive = 0 , IsDefault = true },
                new Campaigns_TB { Id = 4, Name = "Support Conversations ", ProductId = 2, Type = "OFish Preset", IsEnabled = false, Discription = "Support Conversations are started when customers are reaching out for support. By Default customer can receive support via your Chatbot, in-app messaging, or e-mail.", CreatedOn = DateTime.Now, IsActive = 1, IsDefault = true },
                new Campaigns_TB { Id = 5, Name = "Sales Conversations", ProductId = 2, Type = "OFish Preset", IsEnabled = false, Discription = "Sales Conversations are started when leads and unknown users reach out via your chatbot or web forms", CreatedOn = DateTime.Now, IsActive = 1, IsDefault = true }
            );
            #endregion
            #region Campaign Settings
            modelBuilder.Entity<CampaignSettings_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithOne(x => x.CampaignSettings_TB)
                .HasForeignKey<CampaignSettings_TB>(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Campaign Channels
            modelBuilder.Entity<CampaignChannels_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithMany(x => x.CampaignChannels_TBs)
                .HasForeignKey(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Campaign Permessions
            modelBuilder.Entity<CampaignPermissions_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithMany(x => x.CampaignPermissions_TBs)
                .HasForeignKey(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Notifications Settings
            modelBuilder.Entity<NotificationsSettings_TB>()
                .HasOne(x => x.Products_TB)
                .WithMany(x => x.NotificationsSettings_TBs)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<NotificationsSettings_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithMany(x => x.NotificationsSettings_TBs)
                .HasForeignKey(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<NotificationsSettings_TB>()
                .HasOne(x => x.Users_TB)
                .WithMany(x => x.NotificationsSettings_TBs)
                .HasForeignKey(x => x.HubUserId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion
            #region Conversation Status
            modelBuilder.Entity<ConversationStatus_TB>()
                .HasOne(x => x.Products_TB)
                .WithMany(x => x.ConversationStatus_TBs)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ConversationStatus_TB>()
                .HasOne(x => x.Campaigns_TB)
                .WithMany(x => x.ConversationStatus_TBs)
                .HasForeignKey(x => x.CampaignId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ConversationStatus_TB>().HasData(
                new ConversationStatus_TB { Id =1, ProductId = 2, NameInternal = "Open", NamePublic = "Open", Color = "#6fcf97", Action = "Leaves Conversation Open for Replies", IsDefault = true, IsActive= true, CreatedOn= DateTime.Now, IsDefaultForNewMsgs = true },
                new ConversationStatus_TB { Id = 2, ProductId = 2, NameInternal = "Draft", NamePublic = "Draft", Color = "#e0e0e0", Action = "Conversations Not Yet Sent", IsDefault = true, IsActive = true, CreatedOn = DateTime.Now },
                new ConversationStatus_TB { Id = 3, ProductId = 2, NameInternal = "Closed", NamePublic = "Closed", Color = "#4f4f4f", Action = "Closes the Conversation + Prevents Customer Replies unless Re-Opened", IsDefault = true, IsActive = true, CreatedOn = DateTime.Now },
                new ConversationStatus_TB { Id = 4, ProductId = 2, NameInternal = "Closed & Archived", NamePublic = "Closed & Archived", Color = "#4f4f4f", Action = "Once a Conversation has been closed for OFish will Archive it from your Main Inbox.", IsDefault = true, IsActive = true, CreatedOn = DateTime.Now }
            );
            #endregion
        }
    }
}
