﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{

    public class ConversationStatus_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? CampaignId { get; set; }
        public string NameInternal { get; set; }
        public string NamePublic { get; set; }
        public string Action { get; set; }
        public string Color { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public bool IsDefaultForNewMsgs { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        #region Relations 
        public virtual Products_TB Products_TB { get; set; }
        public virtual Campaigns_TB Campaigns_TB { get; set; }
        public virtual ICollection<Rooms_TB> Rooms_TBs { get; set; }

        #endregion
    }
}
