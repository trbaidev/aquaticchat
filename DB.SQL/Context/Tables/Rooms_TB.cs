﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class Rooms_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? StatusId { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        #region Relations
        public int ProductId { get; set; }
        public Products_TB Products_TB { get; set; }
        public ICollection<UserRoom_TB> UserRoom_TBs { get; set; }
        public ICollection<Messages_TB> Messages_TBs { get; set; }
        public int? CampaignId { get; set; }
        public Campaigns_TB Campaigns_TB { get; set; }
        public ConversationStatus_TB  ConversationStatus_TB { get; set; }
        #endregion
    }
}
