﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class Campaigns_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Discription { get; set; }
        public string Image { get; set; }
        public int IsActive { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDefault { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        #region Relations
        public virtual Products_TB Products_TB { get; set; }
        public virtual CampaignSettings_TB CampaignSettings_TB { get; set; }
        public virtual ICollection<CampaignChannels_TB> CampaignChannels_TBs { get; set; }
        public virtual ICollection<CampaignPermissions_TB> CampaignPermissions_TBs { get;set;}
        public virtual ICollection<NotificationsSettings_TB> NotificationsSettings_TBs { get; set; }
        public virtual ICollection<Rooms_TB> Rooms_TBs { get; set; }
        public virtual ICollection<ConversationStatus_TB> ConversationStatus_TBs { get; set; }
        
        #endregion
    }
}
