﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class Products_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string GreetingMsg { get; set; }
        public string GreetingMsgFrom { get; set; }
        public int IsActive { get; set; }
        public bool IsConfigured { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }

        #region Relations
        public ICollection<Rooms_TB> Rooms_TBs { get; set; }
        public ICollection<Users_TB> Users_TBs { get; set; }
        public virtual ICollection<Campaigns_TB> Campaigns_TBs { get; set; }
        public virtual ICollection<NotificationsSettings_TB> NotificationsSettings_TBs { get; set; }
        public virtual ICollection<ConversationStatus_TB> ConversationStatus_TBs { get; set; }
        #endregion
    }
}
