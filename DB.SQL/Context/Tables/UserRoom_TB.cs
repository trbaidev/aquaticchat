﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class UserRoom_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool Is_Favourite { get; set; }
        public int IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }

        public DateTime? DeleteDate { get; set; }
        #region Relations
        public int UserId { get; set; }
        public Users_TB Users_TB { get; set; }

        public int RoomId { get; set; }
        public Rooms_TB Rooms_TB { get; set; }
        #endregion
    }
}
