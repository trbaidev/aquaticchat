﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class Users_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ClientId { get; set; }
        public  string ClientName { get; set; }
        public int IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public string Generic { get; set; }
        #region Relations
        public int ProductId { get; set; }
        public Products_TB Products_TB { get; set; }
        public ICollection<UserRoom_TB> UserRoom_TBs { get; set; } 
        public ICollection<Messages_TB> Messages_TBs { get; set; }
        public ICollection<UserConnection_TB> UserConnection_TBs { get; set; }
        public virtual ICollection<NotificationsSettings_TB> NotificationsSettings_TBs { get; set; }
        #endregion
    }
}
