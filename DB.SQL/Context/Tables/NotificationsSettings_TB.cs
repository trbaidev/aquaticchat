﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.SQL.Context.Tables
{
    public class NotificationsSettings_TB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? CampaignId { get; set; }
        public int? HubUserId { get; set; }
        public bool IsGlobal { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        #region Relations
        public virtual Products_TB Products_TB { get; set; }
        public virtual Campaigns_TB Campaigns_TB { get; set; }
        public virtual Users_TB Users_TB { get; set; }
        #endregion


    }
}
